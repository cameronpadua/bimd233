var wx_data = [{
  day: "fri",
  hi: 82,
  lo: 55
}, {
  day: "sat",
  hi: 75,
  lo: 52
}, {
  day: "sun",
  hi: 69,
  lo: 52
}, {
  day: "mon",
  hi: 69,
  lo: 48
}, {
  day: "tue",
  hi: 68,
  lo: 51
}]

var averages = null;

function getAverageNums(total, num, index, array) {
  total.hi += num.hi;
  total.lo += num.lo;
  if (index === array.length - 1) {
    retVal = [(total.hi) / 5, total.lo / 5];
    total.hi = 0;
    total.lo = 0;
    return retVal;
  } else {
    return array[0];
  }
}

function getAverage() {
  if (averages == null) {
    averages = wx_data.reduce(getAverageNums);
    //console.log(averages);
    document.getElementById("AvgHiTemp").innerHTML = "The Average High temperature: " + averages[0];
    document.getElementById("AvgLowTemp").innerHTML = "The Average Low temperature: " + averages[1];
  }
}
