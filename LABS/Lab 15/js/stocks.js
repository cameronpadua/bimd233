var companies = [
  {company:"Microsoft" , marketcap:"381.7B" , sales:"86.8B" , profit:"22.1B" , numOfEmployees:128000 },
  {company:"Symetra Financial" , marketcap:"2.7B" , sales:"2.2B" , profit:"254.4M" , numOfEmployees:1400 },
  {company:"Micron Technology" , marketcap:"37.6B" , sales:"16.4B" , profit:"3B" , numOfEmployees:30400 },
  {company:"F5 Networks" , marketcap:"9.5B" , sales:"1.7B" , profit:"311.2M" , numOfEmployees:3834 },
  {company:"Expedia" , marketcap:"10.8B" , sales:"1.7B" , profit:"398.1M" , numOfEmployees:18210 },
  {company:"Nautilus" , marketcap:"476M" , sales:"274.4M" , profit:"18.8M" , numOfEmployees:340 },
  {company:"Heritage Financial" , marketcap:"531M" , sales:"137.6M" , profit:"21M" , numOfEmployees:748 },
  {company:"Cascade Microtech" , marketcap:"239M" , sales:"136M" , profit:"9.9M" , numOfEmployees:449 },
  {company:"Nike" , marketcap:"83.1B" , sales:"27.8B" , profit:"2.7B" , numOfEmployees:56500 },
  {company:"Alaska Air Group" , marketcap:"7.9B" , sales:"5.4B" , profit:"605M" , numOfEmployees:13952 },
];
var table = document.getElementById("tableBody");

function listStocks(item, index){
  if(parseInt(index) === 0) table.innerHTML = "";
  table.innerHTML += "<tr><td>" + (parseInt(index)+1).toString() + "</td><td>" + item.company + "</td><td>" + item.marketcap + "</td><td>" + item.sales + "</td><td>" + item.profit + "</td><td>" + item.numOfEmployees + "</td></tr>";
}
