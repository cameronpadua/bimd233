var table = document.getElementById("myTable");



function calculateRandomCircle() {
  let radius = Math.floor(Math.random() * 20) + 1
  let geo = calcCircleGeometries(radius);

  var row = table.insertRow();
  var cell0 = row.insertCell();
  var cell1 = row.insertCell();
  var cell2 = row.insertCell();
  var cell3 = row.insertCell();
  cell0.innerHTML = radius;
  cell1.innerHTML = geo[0];
  cell2.innerHTML = geo[1];
  cell3.innerHTML = geo[2];

}

function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  var geometries = [area, circumference, diameter];
  return geometries;
}

calculateRandomCircle();
calculateRandomCircle();
calculateRandomCircle();
