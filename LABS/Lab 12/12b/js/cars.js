var table = document.getElementById("myTable");

var cars = [];
var car1 = ["Ford", "Fusion", 2017, 50000]
var car2 = ["Chevy", "Avalanche", 2002, 7500]
var car3 = ["Toyota", "Prius", 2012, 27000]
var car4 = ["Honda", "Accord", 2005, 2000]
var car5 = ["Ferrari", "Vroom", 2017, 2000000]

cars.push(car1);
cars.push(car2);
cars.push(car3);
cars.push(car4);
cars.push(car5);

function insertCars() {
  for(let car of cars){
    var row = table.insertRow();
    var cell0 = row.insertCell();
    var cell1 = row.insertCell();
    var cell2 = row.insertCell();
    var cell3 = row.insertCell();
    cell0.innerHTML = car[0];
    cell1.innerHTML = car[1];
    cell2.innerHTML = car[2];
    cell3.innerHTML = car[3];
  }
}

insertCars();
