var schools =   [{ School: "Washington State", Conference: "7-1", Overall: "10-1",lastgameType: "W", lastgameScore: "69-28 ARIZ", id: "wsu", img: "<img class='img' src='images/wsu.png'>"},
                { School: "Washington", Conference: "6-2", Overall: "8-3", id: "uw", lastgameType: "W", lastgameScore: "42-23 ORST",img: "<img class='img' src='images/uw.png'>"},
                { School: "Stanford", Conference: "4-3", Overall: "6-4", id: "su", lastgameType: "W", lastgameScore: "48-17 ORST",img: "<img class='img' src='images/su.png'>"},
                { School: "Oregon", Conference: "4-4", Overall: "7-4", id: "ou", lastgameType: "W", lastgameScore: "31-29 ASU",img: "<img class='img' src='images/ou.png'>"},
                { School: "California", Conference: "3-4", Overall: "6-4", id: "cal", lastgameType: "W", lastgameScore: "15-14 USC",img: "<img class='img' src='images/cal.png'>"},
                { School: "Oregon State", Conference: "1-7", Overall: "2-9", id: "osu", lastgameType: "L", lastgameScore: "23-42 WASH",img: "<img class='img' src='images/osu.png'>"}];

var el = document.querySelectorAll('li');
var i = 1;
function populate(item, index){
  el[i].id = item.id;
  el[i].innerHTML = "";
  el[i].innerHTML += "<div class='row'><div class='col-sm-2 college'>" + item.img + "</div><div class='col-sm-2 college align-self-center'>" + item.School + "</div><div class='col-sm-4 conference'>" + item.Conference + "</div><div class='col-sm-4 overall'>" + item.Overall + "</div></div>";
  i++;
  if(i == el.length){
    i = 1;
  }
}
