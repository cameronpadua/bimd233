$(document).ready(function() {
  $("li").css("id", "uw");
  const states = ["idle", "gather", "process"];
  var state = states[0];
  var words = new Array();
  var ndx = 0;

  $("ul").on("mouseover", "li", function() {
    console.log("x:" + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function() {
    $(this).attr("id", "uw-gold");
  });

  // reset button click
  $("button").on("click", function(e) {
    $('li').remove();
    words = [];
    state = "idle";
    $('#textBox').val('');
  });

  // keypress
  $("input").on("keypress", function(e) {
    var code = e.which;
    var char = String.fromCharCode(code);
    console.log("state " + state);
    console.log("char  " + char);
    if (code == 13) {
      state = "process";
    } else {
      state = "gather";
    }
    switch (state) {
      // idle
      case "idle":
        break;

        // gather
      case "gather":
        console.log("code " + char);
        words.push(char);
        break;

        // process
      case "process":
        if (words.length != 0) {
          $(".list-group").append('<li>' + words.join('') + '</li>');
          words = [];
          state = "idle";
          $('#textBox').val('');
        }
        break;

      default:
        break;
    }
  });
});
