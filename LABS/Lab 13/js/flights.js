flights = [];
var table = document.getElementById("tableBody");
class Flight {
  constructor(airline, number, origin, destination, departure_time, arrival_time, arrival_gate){
    this.airline = airline;
    this.number = number;
    this.origin = origin;
    this.destination = destination;
    this.departure_time = departure_time;
    this.arrival_time = arrival_time;
    this.gate = arrival_gate;
  }
}

flights.push(new Flight("Southwest", 736, "Denver, CO", "Seattle, WA", new Date('February 27, 2019 13:28:00'), new Date('February 27, 2019 15:13:00'), "B14"))
flights.push(new Flight("United", 759, "San Francisco, CA", "Seattle, WA", new Date('February 27, 2019 13:27:00'),new Date('February 27, 2019 15:14:00'), "A9"))
flights.push(new Flight("SkyWest", 3744, "Spokane, WA", "Seattle, WA", new Date('February 27, 2019 14:10:00'), new Date('February 27, 2019 15:09:00'), "B3"))
flights.push(new Flight("SkyWest", 3495, "Fresno, CA", "Seattle, WA", new Date('February 27, 2019 12:56:00'), new Date('February 27, 2019 15:10:00'), "C10"))
flights.push(new Flight("Horizon", 2216, "Medford, OR", "Seattle, WA", new Date('February 27, 2019 13:51:00'), new Date('February 27, 2019 15:07:00'), "C2"))

for(let flight of flights){
  var milliseconds = flight.arrival_time - flight.departure_time,
    minutes = parseInt((milliseconds / (1000 * 60)) % 60),
    hours = parseInt((milliseconds / (1000 * 60 * 60)) % 24);

  var row = table.insertRow();
  var cell0 = row.insertCell();
  var cell1 = row.insertCell();
  var cell2 = row.insertCell();
  var cell3 = row.insertCell();
  var cell4 = row.insertCell();
  var cell5 = row.insertCell();
  var cell6 = row.insertCell();
  var cell7 = row.insertCell();
  cell0.innerHTML = flight.airline;
  cell1.innerHTML = flight.number;
  cell2.innerHTML = flight.origin;
  cell3.innerHTML = flight.destination;
  cell4.innerHTML = flight.departure_time;
  cell5.innerHTML = flight.arrival_time;
  cell6.innerHTML = flight.gate;
  cell7.innerHTML = hours + " Hours " + minutes + " minutes "
}
