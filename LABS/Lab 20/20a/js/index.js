var myUrl = "https://api.coindesk.com/v1/bpi/currentprice/";
var searchTerm;
var end = ".json";



var chartData = [];
var code = "USD";
var priceChart;
$(document).ready(function() {
  $('#search').on('change', function() {
    searchTerm = $("#search").val();
    searchTerm.replace(" ", "+");
    console.log(searchTerm)
    searchTerm.replace(" ", "+");
  }).change();
  var morris_chart_obj = {
    element: "price-chart",
    data: "",
    xkey: "time",
    ykeys: ["price"]
  };
  $("#hit-me").click(function() {
    getData()
  });


setInterval(function(){
  getData();
}, 3000);

function getData() {
  $.ajax({
    url: myUrl+searchTerm+end,
    success: function(data) {
      console.log(parseData(data));

      printPrice(parseData(data));
      renderGraph(data);
    }
  });
}

function renderGraph(data) {
  // console.log();
  plotMyData(parseData(data));
}

 priceChart = new Morris.Line(morris_chart_obj);

});

function printPrice(data) {
  $('#price').text(data);
}

function parseData(textPriceData) {
  let bcJSON = JSON.parse(textPriceData);
  // let currencyCode = $("span#currency-code").text();
  let currencyCode = "USD";
  let rateText = bcJSON.bpi[currencyCode].rate;
  rateText = rateText.replace(/,/g, "");
  return parseFloat(rateText);
}

function updateData(price) {
  const maxChartValues = 32;
  var newEvent = {
    time: Date.now(),
    price: price
  };

  chartData.push(newEvent);
  if (chartData.length > maxChartValues) {
    chartData.shift();
  }
}

function plotMyData(data) {
  updateData(data);
  priceChart.setData(chartData);
  priceChart.redraw();
}
