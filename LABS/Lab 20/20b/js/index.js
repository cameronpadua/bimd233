var base_URL = "https://newsapi.org/v2/everything?q="
var end = "&sortBy=publishedAt&apiKey=028faff14a0b4151b31d88f7ecec3dcb"
var searchTerm = "";
$(document).ready(function() {
  $("#hit-me").click(function() {
    var req = new Request(returnURL());
    fetch(req)
      .then(function(response) {
        return response.json();
      })
      .then(function(data) {
        console.log(data);
        var cards = "";
        for (var result of data.articles) {
          var card = "<div class=\"card col-sm-3\" style=\"width: 18rem;\">" +
            "<div class=\"card-body\">" +
            "<h5 class=\"card-title\">" + result.title + "</h5>" +
            "<p class=\"card-text\">" + result.description + "</p>" +
            "</p><a href=\"" + result.url + "\" class=\"btn btn-success\">Article</a>" +
            "</div></div>";
          cards += card;
        }
        $("#newsContainer").html(cards);
      })
  });
  $('#search').on('change', function() {
    searchTerm = $("#search").val();
    searchTerm.replace(" ", "+");
    console.log(searchTerm)
  }).change();
});
function returnURL(){
  return base_URL + searchTerm + end;
}
