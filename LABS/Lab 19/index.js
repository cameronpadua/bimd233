$(document).ready(function() {
  const base_url="https://api.weather.gov/stations/";
  const endpoint="/observations/latest";
  const icon_url = "https://api.weather.gov/icons/land/night/%s?size=medium";

  // weather update button click
  $('#getwx').on('click', function(e) {
    var mystation = $('input').val();
    var myurl = base_url + mystation + endpoint;
    $('input#my-url').val(myurl);
    
    // clear out any previous data
    $('ul li').each(function() {
      // enter code to clear each li
    });
      
    console.log("Cleared Elements of UL");
    
    // execute AJAX call to get and render data
    $.ajax({
      url: myurl,
      dataType: "json",
      success: function(data) {
		  console.log(data);
        var tempC= data['properties']['temperature'].value.toFixed(1);
        var tempF = (tempC * 9/5 + 32).toFixed(1);
		var humidity = data['properties']['relativeHumidity'].value.toFixed(1);
		var windDirection = data['properties']['windDirection'].value;
		var windSpeed = (data['properties']['windSpeed'].value * 1.94384).toFixed(1);
		var icon = data['properties']['icon'];
        var desc = data['properties']['textDescription'];
        // uncomment this if you want to dump full JSON to textarea
        var myJSON = JSON.stringify(data);
        $('textarea').val(myJSON);
		
		
		
		
        $('ul').html("<li>Current temperature: " + tempC +"C " + tempF+"F"+"</li>");
        $('ul').append("<li>Humidity: " + humidity +" %RH</li>");
		if(windDirection != null){
			$('ul').append("<li>Current Wind: " + windDirection +" degrees at "+ windSpeed +" kts</li>");
		}else{
			$('ul').append("<li>Current Wind: No Wind</li>");
		}
		
		$('ul').append("<li><img src=\"" + icon + "\" height=\"100\" width=\"100\"><br><span>"+desc+"</span></li>");
        $('ul li').attr('class', 'list-group-item');

        // add additional code here for the Wind direction, speed, weather contitions and icon image
      }
    });
  });
});