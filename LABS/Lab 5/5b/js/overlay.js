var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");
var image = new Image();

image.onload = function() {
    context.drawImage(image, 0, 0, 600, 600);
    lines();
    circle(100, 100)
    cross(300, 300)
    circle(500, 500)
}

lines = function() {
    context.moveTo(200, 20);
    context.lineTo(200, 580);

    context.moveTo(400, 20);
    context.lineTo(400, 580);

    context.moveTo(20, 200);
    context.lineTo(580, 200);

    context.moveTo(20, 400);
    context.lineTo(580, 400);

    context.lineWidth = 5;
    context.stroke();
}

circle = function(locX, locY) {
    context.moveTo(locX + 80, locY);
    context.arc(locX, locY, 80, 0, 2 * Math.PI);
    context.lineWidth = 15;
    context.stroke();
}
cross = function(locX, locY) {
    var offset = 80;
    context.moveTo(locX - offset, locY - offset);
    context.lineTo(locX + offset, locY + offset);

    context.moveTo(locX + offset, locY - offset);
    context.lineTo(locX - offset, locY + offset);
}

image.src = "https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&h=600&w=600"